import remarkGfm from "remark-gfm";
// const remarkGFM = require('remark-gfm')();
// import { Root as RemarkGFMRoot } from 'remark-gfm'
// const remarkGfm  = import('remark-gfm');

import nextMDX from '@next/mdx'

import remarkFrontmatter from 'remark-frontmatter';
// import rehypeHighlight from 'rehype-highlight';


// /** @type {import('next').NextConfig} */
// const nextConfig = {
//   reactStrictMode: true,
//   swcMinify: true,
// }

// // module.exports = nextConfig
// export default nextConfig

// const withMDX = require('@next/mdx')({
//   extension: /\.mdx?$/,
//   options: {
//     remarkPlugins: [remarkGfm],
//     rehypePlugins: [],
//     // If you use `MDXProvider`, uncomment the following line.
//     providerImportSource: "@mdx-js/react",
//   },
// })

// module.exports = withMDX({
//   // Append the default value with md extensions
//   pageExtensions: ['ts', 'tsx', 'js', 'jsx', 'md', 'mdx'],
// })


const withMDX = nextMDX({
  // By default only the .mdx extension is supported.
  extension: /\.mdx?$/,
  options: {
    remarkPlugins: [remarkGfm, remarkFrontmatter],
    // rehypePlugins: [rehypeHighlight],
    providerImportSource: "@mdx-js/react",
  }
})

export default withMDX({
  swcMinify: true,
  reactStrictMode: true,
  // Support MDX files as pages:
  pageExtensions: ['md', 'mdx', 'tsx', 'ts', 'jsx', 'js'],
})
