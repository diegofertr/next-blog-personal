import '../styles/globals.css'
import type { AppProps } from 'next/app'
import MainLayout from '../components/MainLAyout'
import { MDXProvider } from '@mdx-js/react'
import MDXComponents from '../components/MDXComponents'

function MyApp({ Component, pageProps }: AppProps) {

  return (
    <MDXProvider components={MDXComponents}>
      <MainLayout>
        <Component {...pageProps} />
      </MainLayout>
    </MDXProvider>
  )
}

export default MyApp
