import React from 'react'
import CHeader from './CHeader'
import styles from '../styles/Home.module.css'
import Image from 'next/image'

type MainLayoutProps = {
  children: React.ReactNode | React.ReactNode[]
}

const MainLAyout = ({ children }: MainLayoutProps) => {
  return (
    <>
      <header>
        <CHeader />
      </header>
      <main>
        { children }
      </main>
      <footer className={styles.footer}>
        <a
          href="https://diegofertr.netlify.com"
          target="_blank"
          rel="noopener noreferrer"
        >
          Powered by{' '}
          yeyo with ñeq&apos;e
          {/* <span className={styles.logo}>
            <Image src="https://diegofertr.netlify.app/_nuxt/img/logo-dark.af1eec7.png" alt="Vercel Logo" width={72} height={16} />
          </span> */}
        </a>
      </footer>
    </>
  )
}

export default MainLAyout