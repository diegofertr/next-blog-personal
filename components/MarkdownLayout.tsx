import React from 'react'
import styles from '../styles/Markdown.module.css';
import CBanner from './CBanner';
import CHeader from './CHeader';

type MarkdownLayoutProps = {
  children: React.ReactNode | React.ReactNode[]
}

const MarkdownLayout = ({ children }: MarkdownLayoutProps) => {
  return (
    <>
      {/* <header>
        <CHeader />
      </header> */}
      <div className={styles.container}>
        <CBanner />
        <main>
          {children}
        </main>
      </div>
    </>
  )
}

export default MarkdownLayout