import styles from '../styles/Markdown.module.css'

import Highlight, {
  defaultProps,
} from 'prism-react-renderer'

// import draculaTheme from 'prism-react-renderer/themes/dracula'
// import duotoneLightTheme from 'prism-react-renderer/themes/duotoneLight'
// import synthwave84Theme from 'prism-react-renderer/themes/synthwave84' // best
// import shadesOfPurpleTheme from 'prism-react-renderer/themes/shadesOfPurple'
// import okaidiaTheme from 'prism-react-renderer/themes/okaidia'
// import ultraminTheme from 'prism-react-renderer/themes/ultramin'
// import palenightTheme from 'prism-react-renderer/themes/palenight' // best
// import oceanicNextTheme from 'prism-react-renderer/themes/oceanicNext'
// import githubTheme from 'prism-react-renderer/themes/github'
import nightOwlTheme from 'prism-react-renderer/themes/nightOwl'
import React from 'react';
// import nightOwlLightTheme from 'prism-react-renderer/themes/nightOwlLight'

type CodeProps = {
  children: any; // TODO: pendiente
  className: any; // TODO: pendiente
}

function Code({ children, className }: CodeProps) {
  console.log('classname :: ', className);
  const language = className ? className.replace(/language-/, '') : null

  return (
    <Highlight
      {...defaultProps}
      code={children.trim()}
      theme={nightOwlTheme}
      language={language}
    >
      {({
        className,
        style,
        tokens,
        getLineProps,
        getTokenProps,
      }) => (
        <>
          {language && (
            <pre className={`${className} ${styles.boxCode}`} style={style}>
              {tokens.map((line, i) => (
                <div
                  key={i}
                  {...getLineProps({ line, key: i })}
                >
                  {line.map((token, key) => (
                    <span
                      key={key}
                      {...getTokenProps({ token, key })}
                    />
                  ))}
                </div>
              ))}
            </pre>
          )}
          {!language && (
            <code className={`${className} ${styles.lineCode}`} style={style}>
              {/* TODO: pendiente para mas lineas de codigo sin lenguaje? */}
              {tokens[0][0].content}
              {/* {JSON.stringify(tokens[0][0].content)} */}
            </code>
          )}
        </>
      )}
    </Highlight>
  )
}

const MDXComponents = {
  a: (props: any) => <a {...props} className={styles.postLink} />,
  h1: (props: any) => <h1 {...props} className={styles.postTitle} />,
  h2: (props: any) => <h2 {...props} className={styles.postSubtitle} />,
  h3: (props: any) => <h3 {...props} className={styles.postSubtitle3} />,
  p: (props: any) => <p {...props} className={styles.postParagraph} />,
  ul: (props: any) => <ul {...props} className={styles.unorderedList} />,
  ol: (props: any) => <ol {...props} className={styles.orderedList} />,
  table: (props: any) => <table {...props} className={styles.table} />,
  thead: (props: any) => <thead {...props} className={styles.thead} />,
  tbody: (props: any) => <tbody {...props} className={styles.tbody} />,
  th: (props: any) => <th {...props} className={styles.th} />,
  tr: (props: any) => <tr {...props} className={styles.tr} />,
  td: (props: any) => <td {...props} className={styles.td} />,
  blockquote: (props: any) => <blockquote {...props} className={styles.postBlockquote} />,
  code: (props: any) => <Code {...props} />,
  // pre: (props: any) => <pre {...props} className={styles.postPrecode} />,
  // code: (props: any) => <code {...props} className={styles.postCode} />,
}

export default MDXComponents