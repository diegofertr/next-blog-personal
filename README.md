# Blog personal Next

Blog personal, construído con la tecnología de Next + MDX


## Iniciando el proyecto

Primero, instala las dependencias:
```bash
  npm install
```


Y luego ejecuta el servidor de desarrollo:

```bash
  npm run dev
```